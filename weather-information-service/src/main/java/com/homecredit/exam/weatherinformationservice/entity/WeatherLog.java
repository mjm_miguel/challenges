package com.homecredit.exam.weatherinformationservice.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "WeatherLog")
public class WeatherLog {

	@Id
	@GeneratedValue
	private long id;
	
	@Column(name = "RESPONSEID", length = 255)
	private String responseId;
	
	@Column(name = "LOCATION", length = 255)
	private String location;
	
	@Column(name = "ACTUALWEATHER", length = 255)
	private String actualWeather;
	
	@Column(name = "TEMPERATURE", length = 255)
	private String temperature;
	
	@Column(name = "TIMESTAMP")
	private Date timestamp;

	public long getId() {
		return id;
	}

	public String getResponseId() {
		return responseId;
	}

	public String getLocation() {
		return location;
	}

	public String getActualWeather() {
		return actualWeather;
	}

	public String getTemperature() {
		return temperature;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setResponseId(String responseId) {
		this.responseId = responseId;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setActualWeather(String actualWeather) {
		this.actualWeather = actualWeather;
	}

	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
	    int result = 1;
	    
	    result = prime * result + ((location == null) ? 0 : location.hashCode());
	    result = prime * result + ((actualWeather == null) ? 0 : actualWeather.hashCode());
	    result = prime * result + ((temperature == null) ? 0 : temperature.hashCode());
	    
	    return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		
		if (!(obj instanceof WeatherLog)) {
			return false;
		}
		
		WeatherLog weatherLog = (WeatherLog) obj;
		
		if (!this.location.equals(weatherLog.getLocation())) {
			return false;
		}
		
		if (!this.actualWeather.equals(weatherLog.getActualWeather())) {
			return false;
		}
		
		if (!this.temperature.equals(weatherLog.getTemperature())) {
			return false;
		}
		
		return true;
	}

}
