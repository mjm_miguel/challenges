package com.homecredit.exam.weatherinformationservice.mapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.fasterxml.jackson.databind.JsonNode;
import com.homecredit.exam.weatherinformationservice.entity.WeatherLog;

public class WeatherLogMapper {
	
	private final String LIST = "list";
	private final String MAIN = "main";
	private final String NAME = "name";
	private final String TEMP = "temp";
	private final String WEATHER = "weather";
	
	public static final String KEY_VIEW = "view";
	public static final String KEY_DB = "db";
	
	private final int INSERT_LIMIT = 5;
	
	public Map<String, Object> map(JsonNode src) {
		if (null == src) {
			return null;
		}
		
		Map<String, Object> mapResult = new HashMap<String, Object>();
		Map<Integer, WeatherLog> mapWeatherLog = new HashMap<Integer, WeatherLog>();
		List<WeatherLog> lstWeatherLog = new ArrayList<>(10);
		
		int intCounter = 1;
		
		Iterator<JsonNode> iterList = src.get(LIST).iterator();
		while (iterList.hasNext()) {
			JsonNode jsonCity = iterList.next();
			JsonNode jsonMain = jsonCity.get(MAIN);
			
			String strLocation = jsonCity.get(NAME).asText();
			String strTemperature = jsonMain.get(TEMP).asText();
			
			JsonNode weather = jsonCity.get(WEATHER);
			
			if (weather.isNull()) {
				continue;
			}
			
			Iterator<JsonNode> iterWeather = weather.iterator();
			
			while (iterWeather.hasNext()) {
				JsonNode objWeather = iterWeather.next();
				WeatherLog weatherLog = new WeatherLog();
				
				weatherLog.setLocation(strLocation);
				weatherLog.setTemperature(strTemperature);
				weatherLog.setActualWeather(objWeather.get(MAIN).asText());
				weatherLog.setTimestamp(new Date());
				weatherLog.setResponseId(UUID.randomUUID().toString());
				
				if (mapWeatherLog.size() <= INSERT_LIMIT) {
					mapWeatherLog.put(intCounter, weatherLog);
				}
				
				lstWeatherLog.add(weatherLog);
				
				intCounter++;
			}
		}
		
		mapResult.put(KEY_VIEW, lstWeatherLog);
		mapResult.put(KEY_DB, mapWeatherLog);
		
		return mapResult;
	}
}
