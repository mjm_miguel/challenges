package com.homecredit.exam.weatherinformationservice.controller;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.homecredit.exam.weatherinformationservice.entity.WeatherLog;
import com.homecredit.exam.weatherinformationservice.mapper.WeatherLogMapper;
import com.homecredit.exam.weatherinformationservice.repo.WeatherLogRepository;

@RestController
public class WeatherInformationController {
	
	private final String URL = 
			"http://api.openweathermap.org/data/2.5/group?id=2643743,3067696,5391959&appid=66b994ab975fc44036f76a0bb82ea505";
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	WeatherLogRepository weatherLogRepo;
	
	@GetMapping("/wis/display/retrieve")
	public @ResponseBody List<WeatherLog> retrieveFromDB() {
		return weatherLogRepo.findAll();
	}

	@SuppressWarnings("unchecked")
	@GetMapping("/wis/display/rest")
	public @ResponseBody List<WeatherLog> displayWeatherInformation() throws IOException {
		JsonNode responseObject = restTemplate.getForObject(URL, JsonNode.class);
		WeatherLogMapper mapper = new WeatherLogMapper();
		Map<String, Object> mapWeatherLogs = mapper.map(responseObject);
		
		// insert records
		Map<Integer, WeatherLog> insertWeatherLogs = (Map<Integer, WeatherLog>) mapWeatherLogs.get(WeatherLogMapper.KEY_DB);
		Set<Integer> keySet = insertWeatherLogs.keySet();
		Iterator<Integer> iterKeySet = keySet.iterator();
		
		while (iterKeySet.hasNext()) {
			Integer intKey = iterKeySet.next();
			WeatherLog insert = insertWeatherLogs.get(intKey);
			weatherLogRepo.save(insert);
		}
		
		return (List<WeatherLog>) mapWeatherLogs.get(WeatherLogMapper.KEY_VIEW);
	}
}
